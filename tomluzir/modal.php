
<!-- Modal -->
<!-- <div class="modal fade modal-cd" id="cd" tabindex="-1" role="dialog" aria-labelledby="cdTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1>Acesse Nossa <br>Loja Virtual</h1>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="cont-modal">
            <div class="info-modal">
                
                <p>Descubra Vários Produtos disponíveis para compra.</p>
                <ul class="list-unstyled">
                    <li>CD'S</li>
                    <li>Camisetas</li>
                    <li>Bonés</li>
                    <li>Canecas</li>
                    <li>Posters</li>
                    <li>E muito mais</li>
                </ul>
                <a href="">Loja Virtual</a>
            </div>
            <picture class="cd-modal">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/cd.png" alt="">
            </picture>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

$(document).ready(function() {
    $('#cd').modal('show');
})
</script>  -->


<!-- Modal -->
<?php
        $args = array (
        'post_type' => 'agenda',
        );
        $the_query = new WP_Query ( $args );
        ?>
        <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
          <div class="modal fade modal-agenda" id="<?php the_field('id')?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="img-evento">
                    <?php the_post_thumbnail()?>
                  </div>
                  <h2><?php the_title();?></h2>
                  <p><?php the_content()?></p>
                </div>
              </div>
            </div>
          </div>
<?php endwhile; else: endif; ?>