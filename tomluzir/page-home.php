<?php // Template Name: Home ?> 
<?php get_header()?>
<section class="hero" id="home">
    <header>
        <div class="cont-header">
            <div class="logo">
                <picture>
                    <source media="(max-width:600px)" srcset="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/logo-sm.png">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/logo.png" alt="">
                </picture>
            </div>
            <div class="info-menu">
                <!-- <div class="info">
                    <span>Contato para Eventos</span>
                    <a href="tel:992644424" class="num-contato">FONE:(81) 9 9264-4424</a>
                </div> -->
                <div id="myNav" class="overlay">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <div class="overlay-content">
                        <a onclick="closeNav()" href="#home">Principal</a>
                        <a onclick="closeNav()" href="#discografia">Discografia</a>
                        <a onclick="closeNav()" href="#biografia">Biografia</a>
                        <a onclick="closeNav()" href="#agenda">Agenda</a>
                        <a onclick="closeNav()" href="#fotos">Fotos e Vídeos</a>
                        <a onclick="closeNav()" href="#contato">Contato</a>
                    </div>
                </div>
                <div class="menu" onclick="openNav()" onclick="travar()">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/menu.png" alt="">
                </div>
            </div>
        </div>
    </header>
    <div class="hero-content">
        <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active tom-banner">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tom-luzir.png" alt="">
                    <div class="frase-banner">
                       <h1 style="color: #fff;font-size: 2rem;">Poesia, sonho, amor e paixão</h1>
                    </div>
                </div>
                <div class="carousel-item tom-banner">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner-2.png" alt="">
                    <div class="frase-banner">
                       <h1 style="color: #fff;font-size: 2rem;">Poesia, sonho, amor e paixão</h1>
                    </div>
                </div>
                <div class="carousel-item tom-banner">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner-3.png" alt="">
                    <div class="frase-banner">
                       <h1 style="color: #fff;font-size: 2rem;">Poesia, sonho, amor e paixão</h1>
                    </div>
                </div>
            </div>
 
        </div>
    </div>
    <div class="social-hero">
        <!-- <div class="youtube">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/youtube.svg" alt="">
            <iframe width="200" height="150" src="https://www.youtube.com/embed/EpHmbTRqntI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div> -->
        <ul class="redes-sociais list-unstyled">
            <li><a href="https://www.youtube.com/channel/UCKIKTTn2N_X-14rcVQTOGMA"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/youtube-link.png" alt=""></a></li>
            <li><a href="https://www.instagram.com/tomluziroficial/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/instagram-hero.svg" alt=""></a></li>
            <li><a href="https://www.facebook.com/tomluzir" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/facebook.png" alt=""></a></li>
            <li><a href="https://api.whatsapp.com/send?phone=5581992644424&"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/whatsapp.svg" alt=""></a></li>
            <li><a href="https://twitter.com/tomluzir" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/twitter.png" alt=""></a></li>
        </ul>
    </div>
</section>
<!-- Nossos Alguns-->
<section class="albuns" id="discografia">
    <div class="container">
        <h2>Discografia</h2>
        <div class="cont-discografia container">
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="row">
                    <div class="col-md-7 capa-disco">
                        <img class="capa" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/capa-minha-festa.jpg" alt="">
                        <span class="ano">2020</span>
                        <span class="titulo">Minha Festa</span>
                        <span class="autor">Tom Luzir</span>
                    </div>
                    <div class="col-md-4 musicas-albuns">
                        <?php echo do_shortcode( '[ai_playlist id="13"]' ); ?>    
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="row">
                    <div class="col-md-7 capa-disco">
                        <img class="capa" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/capa-balada.jpg" alt="">
                        <span class="ano">2020</span>
                        <span class="titulo">Balada</span>
                        <span class="autor">Tom Luzir</span>
                    </div>
                    <div class="col-md-4 musicas-albuns">
                        <?php echo do_shortcode( '[ai_playlist id="37"]' ); ?>    
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="em-dobro" role="tabpanel" aria-labelledby="nav-contact-tab">
                <div class="row">
                    <div class="col-md-7 capa-disco">
                        <img class="capa" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/capa-duplo.jpg" alt="">
                        <span class="ano">1990 - 2008</span>
                        <span class="titulo">Em Dobro</span>
                        <span class="autor">Tom Luzir</span>
                    </div>
                    <div class="col-md-4 musicas-albuns">
                        <?php echo do_shortcode( '[ai_playlist id="57"]' ); ?>    
                    </div>
                </div>
            
            </div>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                        <div class="row-capas">
                            <div class="cont-capas-nav">
                                <div class="capa-nav">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/capa-pequeno-minha-festa.jpg" alt="">
                                </div>
                                <div class="info-nav">
                                    <span>VER OUTRO ÁLBUM</span>
                                    <span>2020</span>
                                    <span>MINHA Festa - TOM LUZIR</span>
                                </div>
                            </div>
                            <img class="seta-disco" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/SETA.png" alt="">
                        </div>
                    </a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                        <div class="row-capas">
                            <div class="cont-capas-nav">
                                <div class="capa-nav">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/capa-pequeno-balada.jpg" alt="">
                                </div>
                                <div class="info-nav">
                                    <span>VER OUTRO ÁLBUM</span>
                                    <span>2020</span>
                                    <span>BALADA - TOM LUZIR</span>
                                </div>
                            </div>
                            <img class="seta-disco" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/SETA.png" alt="">
                        </div>
                    </a>
                    <a class="nav-item nav-link" id="nav-profile-tab2" data-toggle="tab" href="#em-dobro" role="tab" aria-controls="em-dobro" aria-selected="false">
                        <div class="row-capas">
                            <div class="cont-capas-nav">
                                <div class="capa-nav">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/capa-pequena-duplo.jpg" alt="">
                                </div>
                                <div class="info-nav">
                                    <span>VER OUTRO ÁLBUM</span>
                                     <span>1990 - 2008</span>
                                    <span>EM DOBRO - TOM LUZIR</span>
                                </div>
                            </div>
                            <img class="seta-disco" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/SETA.png" alt="">
                        </div>
                    </a>
                </div>
        </nav>
        </div>
        </div>
    </div>   
</section>
<!-- Sobre-->
<section class="sobre" id="biografia"> 
    <div class="cont-sobre">
        <picture class="img-sobre">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tom-quem-somos.png" alt="">
        </picture>
        <div class="txt-sobre">
            <h2>CANTOR, COMPOSITOR e MÚSICO <br> AUTOR DE MAIS DE 200 MÚSICAS</h2>
            <div class="conteudo">
                <p>Tom Luzir é cantor, compositor, músico e produtor musical. Já compôs mais de 200 belíssimas letras que falam de poesia, sonhos, amor, paixão e felicidade. Nasceu no dia 06/07/1957 em Lago da Pedra, no Maranhão. Em sua adolescência, morava em Timon-MA, onde estudou, cantava no coral da igreja e fez parte de um grupo de teatro.</p>
                <p>Suas referências musicais vieram de sua mãe, Dona Izabel, que era compositora de músicas gospel.</p>        
                <p>Tom Luzir, em Timon, participou de programas de auditório, rádio, festivais e cantou na banda Fantásticos. Com 16 anos, foi morar no Rio de Janeiro e fez algumas apresentações em shows com bandas locais. Entretanto, o seu grande sonho era participar de um festival de música. Inscreveu-se algumas vezes no festival musical MPB-Shell, da Rede Globo em São Paulo.</p>
                <p>Um pouco assustado com a violência no Rio na época, Tom Luzir resolveu morar em São Paulo. Lá, teve que aprender uma profissão. Foi pintor de automóveis, técnico em refrigeração e corretor de imóveis.</p>
                <p>Em 1980, aos 22 anos, casou com Milda e foi pai do seu primeiro filho, Edinho. A música em sua vida foi ficando em segundo plano. Tom Luzir resolveu sair de São Paulo e morar em Recife-PE. Chegando na cidade, montou uma empresa de assistência técnica em refrigeração. Logo em seguida, veio o plano Collor, que afundou o país, e teve que fechar a empresa.</p>    
                <p>Como não podia parar, montou uma imobiliária, www.imovelluxo.com.br. Se especializou em venda, aluguel e consultoria imobiliária. Mas durante todo esse período, Tom Luzir não abandonou a música. Como ele mesmo fala, a música está tatuada em sua mente e alma. Ele precisa dela para viver.</p>
                <p>Em paralelo, Tom Luzir em 1990, lançou o CD Irradiação, com 16 músicas, todas de sua autoria. Um projeto independente, com o nome artístico de JB do Nordeste. Em 1993, casa com Marta e tem seu segundo filho, João Felipe. Em 2008, lançou o CD O seu Sorriso é Alegria é Festa, com 14 músicas, todas de sua autoria. O nome artístico era Tom Jobas. Esse CD teve uma participação de seu filho, João Felipe, que tinha 7 anos, cantando duas músicas (Planeta Azul da Cor do Mar e Minha Professora) também de sua autoria. Mais um projeto independente.</p>
                <p>Em 2015, seu filho João Felipe, com 14 anos, foi classificado para as audições às cegas do The Voice Kids, da Rede Globo. Em 2017, Tom Luzir lançou como produtor seu filho, com o nome artístico de Felipe Luzir www.felipeluzir.com.br, e o álbum Luz do Sol, com 13 músicas, todas de sua autoria. O álbum Luz do Sol se encontra em todas as plataformas digitais.</p>
                <p>Em 2020, Tom Luzir fez uma retrospectiva de toda a sua vida e percebeu que em sua caminhada aprendeu, ensinou e viveu muitas emoções, porém no fundo acha que fez pouco para a humanidade. Então, Tom Luzir decidiu fazer mais, lançou o álbum Minha Festa, com 9 músicas, todas de sua autoria e o álbum Balada, com 9 músicas autorais, a venda em todas as plataformas digitais. 5% de todo o seu faturamento é destinado a ONGs que trabalham com crianças, adolescentes e idosos.</p>
            
            </div>
        </div>
    </div>
</section>
<section class="agenda">
    <div class="container">
        <h2>Agenda</h2>
    </div>
        <?php
            $args = array (
            'post_type' => 'agenda',
            );
            $the_query = new WP_Query ( $args );
            ?>
            <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <div class="cont-agenda container">
                <div class="data">
                    <span><?php the_field('dia')?></span>
                </div> 
                <div class="conteudo">
                    <div class="data-horario">
                        <span><?php the_field('horario')?></span>
                    </div>
                    <h3><?php the_title();?></h3>
                    <p><?php the_excerpt();?></p>
                    <a data-toggle="modal" data-target="#<?php the_field('id')?>">Saiba mais</a>
                </div>
                <div class="img">
                    <?php the_post_thumbnail()?>
                </div>        
            </div>
        <?php endwhile; else: endif; ?>
</section>
<section class="galeria" id="fotos">
    <div class="container">
       <div class="titulos">
            <h2>Fotos e Vídeos</h2> 
            <span><a href="">@tomluziroficial</a></span>
        </div>
        <?php echo do_shortcode( '[insta-gallery id="1"]' ); ?>    
    </div>
</section>

<script>
        $('.owl-carousel').owlCarousel({
        loop:true,
        autoplay:true,
        margin:10,
        responsiveClass:true,
        dots:false,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:1,
            },
            700:{
                items:2,
            },
            1000:{
                items:3,
            }
         }
        })
    </script>

<?php get_footer()?>