<footer id="contato">
    <div class="cont-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 mapa-site">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ftomluzir%2F%3Fref%3Dnf%26hc_ref%3DARTPqshqWl0VZjxXH3XvOwEQXpduIcRVbS7ihSLQjkKAUZJF7_LKL3YqnXFWMbieOU8&tabs&width=340&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                </div>
                <div class="col-md-4 contatos">
                    <h5>Contatos para Shows</h5>
                    <div class="shows">
                        <a href="mail:show@tomluzir.com.br">show@tomluzir.com.br</a>
                    </div>
                    <div class="tel">
                        <p class=>Celular:</p>
                        <a href="tel:81992644424">(81)<strong>9 9264-4424</strong></a>
                    </div>
                    <div class="social-media">
                        <ul class="list-unstyled">
                            <li><a href="https://www.instagram.com/tomluziroficial" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/instagram-hero.svg" alt=""></a></li>
                            <li><a href="https://www.facebook.com/tomluzir" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/facebook.png" alt=""></a></li>
                            <li><a href="https://www.youtube.com/channel/UCKIKTTn2N_X-14rcVQTOGMA" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/youtube.png" alt=""></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 news">
                    <h5>Newsletter</h5>
                    <span>Assine nossa newsletter</span>
                    <div class="group-footer-email">
                        <input type="email" placeholder="Coloque seu email aqui!">
                        <button>Enviar</button>
                    </div>
                    <!-- <div class="social-footer">
                        <div class="social-item">
                            <a href="https://www.twitter.com/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/twitter.png" alt=""></a>
                        </div>
                        <div class="social-item">
                            <a href="https://www.instagram.com/tomluziroficial/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/instagram.png" alt=""></a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="jazzz">
        <div class="container cont-jazzz">
            <span>2020 Ⓒ TOM LUZIR</span>
            <a href="http://jazzz.com.br/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/jazzz.svg" title="Desenvolvido pela Jazzz Agência Digital" alt="Jazzz Agência Digital"></a>
        </div>  
    </div> -->
</footer>
<?php include 'modal.php'; ?>
<script type="text/javascript">
$(window).load(function() {
    $('#ExemploModalCentralizado').modal('show');
});</script>

    <?php wp_footer();?>
    </body>
</html>